FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD target/equipments-service-0.0.1-SNAPSHOT.jar equipments-service-0.0.1-SNAPSHOT.jar
EXPOSE 9006
COPY . .
ENTRYPOINT ["java", "-jar","equipments-service-0.0.1-SNAPSHOT.jar"]