package com.hz.Equipmentsservice.payload;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class FuelSystem {
    private int id;
    private String value;
}
