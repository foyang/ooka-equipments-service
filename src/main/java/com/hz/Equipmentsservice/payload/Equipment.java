package com.hz.Equipmentsservice.payload;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Equipment {
    private String oilSystem;
    private String fuelSystem;
    private String gearboxOptions;
}
