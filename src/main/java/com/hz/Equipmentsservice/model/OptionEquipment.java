package com.hz.Equipmentsservice.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class OptionEquipment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;

    @JsonManagedReference
    @OneToMany(mappedBy = "optionEquipment")
    @Setter @Getter
    private Set<EquipmentConfig> equipmentConfigs;

    public OptionEquipment() {}
}
