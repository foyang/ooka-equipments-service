package com.hz.Equipmentsservice.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


@Entity
@Getter
@Setter
public class EquipmentConfig {
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private  int id;
   private String value;

   @JsonBackReference
   @ManyToOne
   @JoinColumn(name="optionEquipment_id",nullable = false)
   @Setter @Getter
   private OptionEquipment optionEquipment;

   public EquipmentConfig() {}
}
