package com.hz.Equipmentsservice.repository;

import com.hz.Equipmentsservice.model.OptionEquipment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OptionEquipmentRepository extends JpaRepository<OptionEquipment,Integer> {
    OptionEquipment findById(int id);
    OptionEquipment findByName(String name);
}
