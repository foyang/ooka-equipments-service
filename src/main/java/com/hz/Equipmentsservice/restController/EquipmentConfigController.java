package com.hz.Equipmentsservice.restController;

import com.hz.Equipmentsservice.model.EquipmentConfig;
import com.hz.Equipmentsservice.model.OptionEquipment;
import com.hz.Equipmentsservice.repository.EquipmentConfigRepository;
import com.hz.Equipmentsservice.repository.OptionEquipmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("equipment-config")
public class EquipmentConfigController {

    @Autowired
    private EquipmentConfigRepository equipmentConfigRepository;

    @Autowired
    private OptionEquipmentRepository optionEquipmentRepository;

    @PostMapping("{id}/add")
    public EquipmentConfig saveEquipmentConfig(@PathVariable("id") int id, @RequestBody EquipmentConfig equipmentConfig){
        if (equipmentConfig != null){
            OptionEquipment optionEquipment = optionEquipmentRepository.findById(id);
            List<EquipmentConfig> equipmentConfig1 = equipmentConfigRepository.findByValue(equipmentConfig.getValue());
            if (optionEquipment != null && equipmentConfig1.size() == 0){
                equipmentConfig.setOptionEquipment(optionEquipment);
                return equipmentConfigRepository.save(equipmentConfig);
            }
        }
        return null;
    }

    @GetMapping("all")
    public List<EquipmentConfig> getEquipments(){
        return equipmentConfigRepository.findAll();
    }

    @GetMapping("{id}")
    public EquipmentConfig getEquipmentById(@PathVariable("id") int id){
        return equipmentConfigRepository.findById(id);
    }
}
