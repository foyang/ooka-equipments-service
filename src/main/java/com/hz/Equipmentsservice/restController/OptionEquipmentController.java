package com.hz.Equipmentsservice.restController;

import com.hz.Equipmentsservice.model.OptionEquipment;
import com.hz.Equipmentsservice.repository.OptionEquipmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("option-equipment")
public class OptionEquipmentController {

    @Autowired
    private OptionEquipmentRepository optionEquipmentRepository;

    @PostMapping("add")
    public OptionEquipment saveOptionEquipmment(@RequestBody OptionEquipment optionEquipment){
        OptionEquipment equipment = optionEquipmentRepository.findByName(optionEquipment.getName());
        if (equipment == null){
            return optionEquipmentRepository.save(optionEquipment);
        }
        return null;
    }

    @GetMapping("{id}")
    public OptionEquipment getOptionaEquipmentById(@PathVariable("id") int id){
        return optionEquipmentRepository.findById(id);
    }

    @GetMapping("all")
    public List<OptionEquipment> getAllOptionEquipment(){
        return optionEquipmentRepository.findAll();
    }

    @GetMapping("test")
    public String apiTest(){
        return "Equipement service work well";
    }
}
