package com.hz.Equipmentsservice.restController;

import com.hz.Equipmentsservice.payload.Equipment;
import com.hz.Equipmentsservice.payload.FuelSystem;
import com.hz.Equipmentsservice.payload.OilSystem;
import com.hz.Equipmentsservice.payload.Valid;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
@CrossOrigin
@RequestMapping("service-equipment")
public class ServiceCommunicationController {
    @Value("${config.service.timout}")
    private Long duration;

    RestTemplate restTemplate = new RestTemplate();

    @PostMapping("add")
    public ResponseEntity<Equipment> saveEqupment(@RequestBody Equipment equipment){
        try {
            Thread.sleep(duration);
            HttpEntity<Equipment> request = new HttpEntity<>(equipment);
            restTemplate.postForEntity("http://localhost:9002/equipment/add", request, Equipment.class);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @PostMapping("valid/oil")
    public ResponseEntity<Valid> checkValidOil(@RequestBody OilSystem oilSystem){
        try {
            Thread.sleep(duration);
            HttpEntity<OilSystem> request = new HttpEntity<>(oilSystem);
            return restTemplate.postForEntity("http://localhost:9003/oil-system/valid",request, Valid.class);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    return null;
    }

    @PostMapping("valid/fuel")
    public ResponseEntity<Valid> checkValidFuel(@RequestBody FuelSystem fuelSystem){
        try {
            Thread.sleep(duration);
            HttpEntity<FuelSystem> request = new HttpEntity<>(fuelSystem);
            return restTemplate.postForEntity("http://localhost:9004/fuel-system/valid",request,Valid.class);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }
}
